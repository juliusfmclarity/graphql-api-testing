## What needs to be done

- Mutation tests

## GraphQL API Testing

This appliation has been designed to test GraphQL API endpoints from a client perspective.

In order for testing to work, we must download:
- V1 of the web app - `apollo`
- The graphql server - `fmcgraphql`
- The testing framework - `graphql-api-testing`

## Initial Installation

Download and setup the graphql server:
`git clone git@bitbucket.org:fmclarity/fmcgraphql.git`
`cd fmcgraphql` - Go into the fmcgraphql folder.
`git checkout permissions-client-tests` - Go to the permissions-client-tests branch
`meteor npm install` - Install dependencies

Download and setup V1 of the application:
`git clone git@bitbucket.org:fmclarity/apollo.git`
`cd apollo` - Go into the apollo folder.
`git checkout integration` - Go to the integration branch
`meteor npm install` - Install dependencies

Once V1 is setup, you need to create an `env.sh` file with these variables:

`export MONGO_URL="mongodb://127.0.0.1:3001/meteor"`
`PORT=4000`

(NOTE: please note that your mongo url may differ. The mongo url is the url of the mongo database running on the GraphQL server)

Download and setup the testing repository:
`git clone git@bitbucket.org:juliusfmclarity/graphql-api-testing.git` - Clone the files
`cd graphql-api-testing` - Go into the graphql-api-testing folder
`git checkout master` - Go to the master branch (it should be default)
`npm install` - Install dependencies

## Running the tests

In order to run the tests you must first start the graphql server and populate the database:

`cd fmcgraphql` - Go into the fmcgraphql folder
`npm start` - Start the server

While the server is running, in a separate terminal:

`cd fmcgraphql` - Go into the fmcgraphql folder
`meteor shell` - Open the meteor shell
`Meteor.call('database.reset')` - Reset and repopulate the database

In addition, we must also run and setup V1 of the app:

`cd apollo` - Go into the apollo folder
`npm start` - Start the server

Leaving the server + V1 running in the initial terminal, go to your graphql-api-testing repository:

`cd graphql-api-testing` - Go into the graphql-api-testing folder
`npm run test` - Run the tests

## Database

When populating the database with `Meteor.call('database.reset')`, it creates a few things:

- A team. `name: Primary Team, type: fm`
- A user of each role type. (attached to the team)
- A primary facility. `name: Property Manager - Primary Facility Name`
- Five random facilities.`name: random`
- A primary request. (attached to the primary facility) `name: Property Manager - Primary Request Name`
- Five random requests. (attached to the primary facility) `name: random`
- A primary document. (attached to the primary facility) `name: Property Manager - Primary Document Name`
- Five random documents. (attached to the primary facility) ``
- Five random messages. (not yet attached.)

The property manager is the owner of everything.

## File Structure

- src/
    - graphql/ - `contains all graphql setup and query files`
    - tests/ - `contains all tests`
    - index.js - `part of create-react-app. serves no purpose for tests`
    - testConfiguration.js - `global test variables and test functions`

## Configuration File // Global Variables

All configuration details are contained within the `testConfiguration.js` file. It contains a few global functions.

- global.apollo - `the ApolloClient object of the user being tested`
- global.apolloAdmin - `the ApolloClient object of an FMC Support user (in order to generate objects)`
- global.token - `authorization token`
- global.tokenAdmin - `FMC Support authorization token`

- global.fmcSupportEmail
- global.portfolioManagerEmail
- global.facilityManagerEmail
- global.propertyManagerEmail
- global.staffEmail
- global.supplierManagerEmail
- global.supplierEmail

- global.plainPassword - `password for all accounts`

## Configuration File // Global Functions

- global.runBeforeAll(email, password) - `logs the user and FMC Support`
- global.runAfterAll() - `logs the user and FMC Support out`

- global.getPrimaryFacilityId() - `get the primary facility id. uses the FMC Support apollo object`
- global.getPrimaryRequestId() - `gets the primary request id. uses the FMC Support apollo object`

## Additional Notes

- The GraphQL folder and ApolloClient setup has been taken directly from the mobile and web application. The only thing that's slightly unique is the client.js and clientClass.js files.
- In order to for Apollo to work with Jest, we have used the `jest-transform-graphql` node module, given Jest doesn't parse Webpack. It is also imperative to eject create-react-app in order to put into the `package.json`:

```
    "\\.(gql|graphql)$": "jest-transform-graphql",
    ".*": "babel-jest"
```

- I have had to perform some kind of exhaustive work around in order to get the authorization token to work. Basically, mocking localStorage doesn't work, because it originally the ApolloClient configuration existed outside of the scope of Jest. Instead, we're relying on `clientClass.js` which encapulates ApolloClient, which makes the information portable.

## Possible errors

I was getting errors like below:

`node[1515] (FSEvents.framework) FSEventStreamStart: register_with_server: ERROR: f2d_register_rpc() => (null) (-22)`

The fix was to do `brew install watchman` and everything seemed to work fine.

## Branches

There are two branches.

- master (`main working branch`)
- local-mongodb (`experimental branch with local mongodb server setup`)
