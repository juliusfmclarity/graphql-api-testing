import Client from './graphql/client';

import loginWithPlainPasswordMutation from './graphql/mutations/authentication/loginWithPlainPasswordMutation.graphql';
import logoutMutation from './graphql/mutations/authentication/logoutMutation.graphql';

import facilitiesQuery from './graphql/queries/facilities/facilitiesQuery.graphql';
import requestsQuery from './graphql/queries/requests/requestsQuery.graphql';

global.fmcSupportEmail = 'primaryFmcSupport@email.com';
global.portfolioManagerEmail = 'primaryPortfolioManager@email.com';
global.facilityManagerEmail = 'primaryFacilityManager@email.com';
global.propertyManagerEmail = 'primaryPropertyManager@email.com';
global.staffEmail = 'primaryStaff@email.com';
global.supplierManagerEmail = 'primarySupplierManager@email.com';
global.supplierEmail = 'primarySupplier@email.com';

global.plainPassword = '0p3n5354m3';

global.runBeforeAll = (email, plainPassword) => beforeAll(async () => {
  try {
    global.apollo = new Client('');
    global.apolloAdmin = new Client('');

    const { data } = await global.apollo.client.mutate({
      variables: { email, plainPassword },
      mutation: loginWithPlainPasswordMutation,
    });

    global.apollo = new Client(data.loginWithPassword.token);
    global.token = data.loginWithPassword.token;

    const adminResponse = await global.apolloAdmin.client.mutate({
      variables: { email: global.fmcSupportEmail, plainPassword: global.plainPassword },
      mutation: loginWithPlainPasswordMutation,
    });

    global.apolloAdmin = new Client(adminResponse.data.loginWithPassword.token);
    global.tokenAdmin = adminResponse.data.loginWithPassword.token;

  } catch(error) {
    console.log(error);
  };
});

global.runAfterAll = () => afterAll(async () => {
  try {
    await global.apollo.client.mutate({
      mutation: logoutMutation,
      variables: { token: global.token },
    });

    await global.apolloAdmin.client.mutate({
      mutation: logoutMutation,
      variables: { token: global.tokenAdmin },
    });

  } catch(error) {
    console.log(error);
  };
});

// Retrieve Facility, Request _id field

global.getPrimaryFacilityId = async () => {
  // The first facility is always the primary facility.
  try {
    const { data } = await global.apolloAdmin.client.query({
      query: facilitiesQuery,
    });
    return data.facilities[0]._id;
  } catch(error) {
    console.log(error);
  };
};

global.getPrimaryRequestId = async () => {
  // The first request is always the primary request.
  try {
    const { data } = await global.apolloAdmin.client.query({
      query: requestsQuery,
    });
    return data.requests[0]._id;
  } catch(error) {
    console.log(error);
  };
};
