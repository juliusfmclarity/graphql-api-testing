import { ApolloClient } from 'apollo-client';
import { setContext } from 'apollo-link-context';
import { HttpLink } from 'apollo-link-http';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import fetch from 'node-fetch';

import fragmentTypes from './fragmentTypes.json';

class Client {

  constructor(token) {
    this.apolloClient(token);
  }

  authLink(token) {
    return setContext((_, { headers }) => {
      return {
        headers: {
          ...headers,
          Authorization: token ? `Bearer ${token}` : '',
        }
      }
    });
  }

  fragmentMatcher() {
    return new IntrospectionFragmentMatcher({
      introspectionQueryResultData: fragmentTypes
    });
  }

  httpLink() {
    return new HttpLink({
      uri: process.env.REACT_APP_GRAPHQL_URI || 'https://fmclarity-graphql-test-3654.nodechef.com/graphql',
      fetch
    });
  }

  defaultOptions() {
    return {
      defaultOptions: {
        query: {
          fetchPolicy: 'network-only',
          errorPolicy: 'all'
        },

        mutate: {
          errorPolicy: 'all'
        }
      },
    }
  }

  apolloClient(token) {
    this.apollo = new ApolloClient({
      cache: new InMemoryCache({ fragmentMatcher: this.fragmentMatcher() }),
      link: this.authLink(token).concat(this.httpLink()),
      defaultOptions: this.defaultOptions(),
    });
  }

  get client() {
    return this.apollo;
  }
}

export default Client;