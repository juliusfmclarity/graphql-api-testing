// Queries
import meQuery from '../../../graphql/queries/users/meQuery.graphql';

describe('Test "Facility Manager" user permissions on user queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('meQuery - Facility Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: meQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.role).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
