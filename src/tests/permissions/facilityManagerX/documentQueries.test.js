// Queries
import documentQuery from '../../../graphql/queries/documents/documentQuery.graphql';

describe('Test "Facility Manager" user permissions on document queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('documentQuery - Facility Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: documentQuery,
        variables: {
          documentId: 'yv6tNRZSfYnMsaxBr', // Int
        },
      });

      expect(data.document).toBeDefined();
      
    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error.message);
    };
  });

});