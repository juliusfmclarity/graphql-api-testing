// Queries
import TeamMembersQuery from '../../../graphql/queries/teams/TeamMembersQuery.graphql';

describe('Test "Facility Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('TeamMembersQuery - Facility Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: TeamMembersQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.team).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
