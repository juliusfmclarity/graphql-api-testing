// Queries
import messagesQuery from '../../../graphql/queries/messages/messagesQuery.graphql';

describe('Test "Facility Manager" user permissions on message queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('messagesQuery - Facility Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: messagesQuery,
        // variables: {
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });

      expect(data.messages).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error.message);
    };
  });

});