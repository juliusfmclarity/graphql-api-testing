// Queries
import requestsQuery from '../../../graphql/queries/requests/requestsQuery.graphql';
import requestQuery from '../../../graphql/queries/requests/requestQuery.graphql';
import requestDocumentsQuery from '../../../graphql/queries/requests/requestDocumentsQuery.graphql';
import requestMessagesQuery from '../../../graphql/queries/requests/requestMessagesQuery.graphql';

describe('Test "Facility Manager" user permissions on request queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('requestsQuery - Facility Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: requestsQuery,
        // variables: {
        // facilityId: primaryFacilityId, // ID
        //   type: '', // String
        //   status: '', // RequestStatus
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });

      expect(data.requests).toBeDefined();
      expect(data.requests.length).toBeGreaterThan(1);

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  test('requestQuery - Facility Manager - should return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data, error } = await global.apollo.client.query({
        query: requestQuery,
        variables: {
          _id: primaryRequestId, // ID
        }
      });

      expect(data.request).toBeDefined();
    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  test('requestDocumentsQuery - Facility Manager - should return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data, error } = await global.apollo.client.query({
        query: requestDocumentsQuery,
        variables: {
          requestId: primaryRequestId, // ID!
        //   types: '', // [DocumentType]
        //   service: '', // String
        //   subService: '', // String
        //   offset: '', // Int
        //   limit: '', // Int
        }
      });

      expect(data.request).toBeDefined();
      expect(data.request.documents).toBeDefined();
      expect(data.request.documentsCount).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  test('requestMessagesQuery - Facility Manager - should return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data, error } = await global.apollo.client.query({
        query: requestMessagesQuery,
        variables: {
          _id: primaryRequestId, // ID!
          // offset: '', // Int
          // limit: '', // Int
        }
      });

      expect(data.request).toBeDefined();
      expect(data.request.messagesCount).toBeDefined();
      expect(data.request.messages).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});

