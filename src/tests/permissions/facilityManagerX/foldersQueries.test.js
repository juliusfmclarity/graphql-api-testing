// Queries
import foldersQuery from '../../../graphql/queries/folders/foldersQuery.graphql';

describe('Test "Facility Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('foldersQuery - Facility Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data, error } = await global.apollo.client.query({
        query: foldersQuery,
        variables: {
          facilityId: primaryFacilityId,
        },
      });

      expect(data.folders).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
