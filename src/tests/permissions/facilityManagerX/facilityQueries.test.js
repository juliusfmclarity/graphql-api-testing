// Queries
import facilitiesQuery from '../../../graphql/queries/facilities/facilitiesQuery.graphql';
import facilityQuery from '../../../graphql/queries/facilities/facilityQuery.graphql';
import facilitiesDocumentsCountQuery from '../../../graphql/queries/facilities/facilitiesDocumentsCountQuery.graphql';
import facilityDocumentsQuery from '../../../graphql/queries/facilities/facilityDocumentsQuery.graphql';

describe('Test "Facility Manager" user permissions on facility queries.', () => {

  global.runBeforeAll(global.facilityManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('facilitiesQuery - Facility Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: facilitiesQuery,
        // variables: {
        //   offset: '', // Int
        //   limit: '', // Int
        //   isBookable: '', // Boolean
        // }
      });

      expect(data.facilities).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  it('facilityQuery - Facility Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.query({
        query: facilityQuery,
        variables: {
          _id: primaryFacilityId, // ID!
        },
      });

      expect(data.facility).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  it('facilitiesDocumentsCountQuery - Facility Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: facilitiesDocumentsCountQuery,
        // variables: {
        //   types: '', // [DocumentType]
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });

      expect(data.facilities).toBeDefined();
      expect(data.facilities[0].documentsCount).toBeDefined();
      expect(data.facilitiesCount).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  it('facilityDocumentsQuery - Facility Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data, error } = await global.apollo.client.query({
        query: facilityDocumentsQuery,
        variables: {
          facilityId: primaryFacilityId, // ID!
          // types: '', // [DocumentType]
          // service: '', // String
          // subService: '', // String
          // offset: '', // Int
          // limit: '', // Int
        },
      });

      expect(data.facility).toBeDefined();
      expect(data.facility.documents).toBeDefined();
      expect(data.facility.documentsCount).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    }
  });
});

