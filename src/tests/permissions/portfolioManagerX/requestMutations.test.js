// Queries
// import requestQuery from '../../../graphql/queries/requests/requestQuery.graphql';
// import requestsQuery from '../../../graphql/queries/requests/requestsQuery.graphql';

// Mutations
import createRequestMutation from '../../../graphql/mutations/requests/createRequestMutation.graphql';
import assignRequest from '../../../graphql/mutations/requests/assignRequest.graphql';
import completeRequest from '../../../graphql/mutations/requests/completeRequest.graphql';
import issueRequestMutation from '../../../graphql/mutations/requests/issueRequestMutation.graphql';
import updateRequestMutation from '../../../graphql/mutations/requests/updateRequestMutation.graphql';
import deleteRequestMutation from '../../../graphql/mutations/requests/deleteRequestMutation.graphql';

describe('Test "Portfolio Manager" user permissions on request mutations.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('createRequestMutation - Portfolio Manager - should create and delete request', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.mutate({
        mutation: createRequestMutation,
        variables: {
          facilityId: primaryFacilityId, // ID!
          request: { name: 'Default Request Name' }, // RequestInput!
        },
      });

      expect(data.createRequest).toBeDefined();
    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });

  // test('assignRequest - Portfolio Manager - should X', async () => {
  //   try {
  //     const createResponse = await global.apollo.client.mutate({
  //       mutation: createRequestMutation,
  //       variables: {
  //         facilityId: '', // ID!
  //         request: '', // RequestInput!
  //       }
  //     });

  //     expect(createResponse.data.request).toBeDefined();
  //     // check to see if issued field is defined.
  //     expect(createResponse.error.request).toBeUndefined();

  //     const mutateResponse = await global.apollo.client.mutate({
  //       mutation: assignRequest,
  //       variables: {
  //         _id: '', // ID!
  //         assigneeId: '', // ID!
  //       }
  //     });

  //     expect(mutateResponse.data.request).toBeDefined();
  //     expect(mutateResponse.error.request).toBeUndefined();

  //   } catch(error) {
  //     console.log(error);
  //   }
  // });

  // test('completeRequest - Portfolio Manager - should X', async () => {
  //   try {
  //     const createResponse = await global.apollo.client.mutate({
  //       mutation: createRequestMutation,
  //       variables: {
  //         facilityId: '', // ID!
  //         request: '', // RequestInput!
  //       }
  //     });

  //     expect(createResponse.data.request).toBeDefined();
  //     // check to see if issued field is defined.
  //     expect(createResponse.error.request).toBeUndefined();

  //     const mutateResponse = await global.apollo.client.mutate({
  //       mutation: completeRequest,
  //       variables: {
  //         _id: '', // ID!
  //         closeDetails: '', // RequestCloseDetailsInput!
  //       }
  //     });

  //     expect(mutateResponse.data.request).toBeDefined();
  //     expect(mutateResponse.error.request).toBeUndefined();

  //   } catch(error) {
  //     console.log(error);
  //   }
  // });

  // test('issueRequestMutation - Portfolio Manager - should X', async () => {
  //   try {
  //     const createResponse = await global.apollo.client.mutate({
  //       mutation: createRequestMutation,
  //       variables: {
  //         facilityId: '', // ID!
  //         request: '', // RequestInput!
  //       }
  //     });

  //     expect(createResponse.data.request).toBeDefined();
  //     expect(createResponse.error.request).toBeUndefined();

  //     const mutateResponse = await global.apollo.client.mutate({
  //       mutation: issueRequestMutation,
  //       variables: {
  //         id: '', // ID!
  //       }
  //     });

  //     expect(mutateResponse.data.request).toBeDefined();
  //     expect(mutateResponse.error.request).toBeUndefined();

  //   } catch(error) {
  //     console.log(error);
  //   }
  // });
});

