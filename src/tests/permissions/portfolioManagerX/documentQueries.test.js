// Queries
import documentQuery from '../../../graphql/queries/documents/documentQuery.graphql';

describe('Test "Portfolio Manager" user permissions on document queries.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('documentQuery - Portfolio Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: documentQuery,
        variables: {
          documentId: 'yv6tNRZSfYnMsaxBr', // Int
        },
      });

      expect(data.document).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error.message);
    };
  });

});