// Queries
import foldersQuery from '../../../graphql/queries/folders/foldersQuery.graphql';

describe('Test "Portfolio Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('foldersQuery - Portfolio Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.query({
        query: foldersQuery,
        variables: {
          facilityId: primaryFacilityId,
        },
      });

      expect(data.folders).toBeDefined();
    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
