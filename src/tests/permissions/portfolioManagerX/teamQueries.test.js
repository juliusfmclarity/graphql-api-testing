// Queries
import TeamMembersQuery from '../../../graphql/queries/teams/TeamMembersQuery.graphql';

describe('Test "Portfolio Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('TeamMembersQuery - Portfolio Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: TeamMembersQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.team).toBeDefined();
      
    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
