// Queries
import meQuery from '../../../graphql/queries/users/meQuery.graphql';

describe('Test "Portfolio Manager" user permissions on user queries.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('meQuery - Portfolio Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: meQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.role).toBeDefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
