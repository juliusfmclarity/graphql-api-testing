// Queries
import messagesQuery from '../../../graphql/queries/messages/messagesQuery.graphql';

describe('Test "Portfolio Manager" user permissions on message queries.', () => {

  global.runBeforeAll(global.portfolioManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('messagesQuery - Portfolio Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: messagesQuery,
        // variables: {
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });

      expect(data.messages).toBeDefined();
      expect(data.messages.length).toBeGreaterThan(0);

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error.message);
    };
  });

});