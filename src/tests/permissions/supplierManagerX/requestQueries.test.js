// Queries
import requestsQuery from '../../../graphql/queries/requests/requestsQuery.graphql';
import requestQuery from '../../../graphql/queries/requests/requestQuery.graphql';
import requestDocumentsQuery from '../../../graphql/queries/requests/requestDocumentsQuery.graphql';
import requestMessagesQuery from '../../../graphql/queries/requests/requestMessagesQuery.graphql';

describe('Test "Supplier Manager" user permissions on request queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('requestsQuery - Supplier Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: requestsQuery,
        // variables: {
        // facilityId: '', // ID
        //   type: '', // String
        //   status: '', // RequestStatus
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });

      expect(data.requests).toBeDefined();
      expect(data.requests.length).toBeGreaterThan(0);

    } catch(error) {
      expect(error).toBeUndefined();
    };
  });

  test('requestQuery - Supplier Manager - should NOT return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data } = await global.apollo.client.query({
        query: requestQuery,
        variables: {
          _id: primaryRequestId, // ID
        },
      });

      expect(data.request).toBeUndefined();
    } catch(error) {
      expect(error.message).toContain('Access denied');
      expect(error).toBeDefined();
    };
  });

  test('requestDocumentsQuery - Supplier Manager - should NOT return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data } = await global.apollo.client.query({
        query: requestDocumentsQuery,
        variables: {
          requestId: primaryRequestId, // ID!
        //   types: '', // [DocumentType]
        //   service: '', // String
        //   subService: '', // String
        //   offset: '', // Int
        //   limit: '', // Int
        }
      });
      expect(data.request).toBeUndefined();
      expect(data.request.documents).toBeUndefined();
      expect(data.request.documentsCount).toBeUndefined();
    } catch(error) {
      expect(error.message).toContain('Access denied');
      expect(error).toBeDefined();
    };
  });

  test('requestMessagesQuery - Supplier Manager - should NOT return data', async () => {
    try {
      const primaryRequestId = await global.getPrimaryRequestId();
      const { data } = await global.apollo.client.query({
        query: requestMessagesQuery,
        variables: {
          _id: primaryRequestId, // ID!
          // offset: '', // Int
          // limit: '', // Int
        }
      });

      expect(data.request).toBeUndefined();
      expect(data.request.messages).toBeUndefined();
      expect(data.request.messagesCount).toBeUndefined();
    } catch(error) {
      expect(error.message).toContain('Access denied');
      expect(error).toBeDefined();
    };
  });
});

