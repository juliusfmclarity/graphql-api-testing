// Queries
import meQuery from '../../../graphql/queries/users/meQuery.graphql';

describe('Test "Supplier Manager" user permissions on user queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('meQuery - Supplier Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: meQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.role).toBeDefined();
      expect(error).toBeUndefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
