// Queries
import documentQuery from '../../../graphql/queries/documents/documentQuery.graphql';

describe('Test "Supplier Manager" user permissions on document queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('documentQuery - Supplier Manager - should NOT return data', async () => {
    try {
      await global.apollo.client.query({
        query: documentQuery,
        variables: {
          documentId: 'yv6tNRZSfYnMsaxBr', // Int
        },
      });
      expect(data.document).toBeUndefined();
    } catch(error) {
      expect(error.message).toContain('Access denied');
      expect(error).toBeDefined();
    };
  });
});