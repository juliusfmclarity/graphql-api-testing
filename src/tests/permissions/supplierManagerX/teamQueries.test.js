// Queries
import TeamMembersQuery from '../../../graphql/queries/teams/TeamMembersQuery.graphql';

describe('Test "Supplier Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('TeamMembersQuery - Supplier Manager - should return data', async () => {
    try {
      const { data, error } = await global.apollo.client.query({
        query: TeamMembersQuery,
      });

      expect(data.me).toBeDefined();
      expect(data.me.team).toBeDefined();
      expect(error).toBeUndefined();

    } catch(error) {
      expect(error).toBeUndefined();
      console.log(error);
    };
  });
});
