// Queries
import foldersQuery from '../../../graphql/queries/folders/foldersQuery.graphql';

describe('Test "Supplier Manager" user permissions on team queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('foldersQuery - Supplier Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.query({
        query: foldersQuery,
        variables: {
          facilityId: primaryFacilityId,
        },
      });
      expect(data.folders).toBeDefined();
      console.log(data.folders);
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    };
  });
});
