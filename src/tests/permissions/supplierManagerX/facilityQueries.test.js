// Queries
import facilitiesQuery from '../../../graphql/queries/facilities/facilitiesQuery.graphql';
import facilityQuery from '../../../graphql/queries/facilities/facilityQuery.graphql';
import facilitiesDocumentsCountQuery from '../../../graphql/queries/facilities/facilitiesDocumentsCountQuery.graphql';
import facilityDocumentsQuery from '../../../graphql/queries/facilities/facilityDocumentsQuery.graphql';

describe('Test "Supplier Manager" user permissions on facility queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('facilitiesQuery - Supplier Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: facilitiesQuery,
        // variables: {
        //   offset: '', // Int
        //   limit: '', // Int
        //   isBookable: '', // Boolean
        // }
      });
      expect(data.facilities).toBeDefined();
      expect(data.facilities.length).toBeGreaterThan(0);
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    };
  });

  it('facilityQuery - Supplier Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.query({
        query: facilityQuery,
        variables: {
          _id: primaryFacilityId, // ID!
        },
      });
      expect(data.facility).toBeDefined();
      expect(data.facility.areas).toBeDefined();
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    };
  });

  it('facilitiesDocumentsCountQuery - Supplier Manager - should return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: facilitiesDocumentsCountQuery,
        // variables: {
        //   types: '', // [DocumentType]
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });
      expect(data.facilities).toBeDefined();
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    };
  });

  it('facilityDocumentsQuery - Supplier Manager - should return data', async () => {
    try {
      const primaryFacilityId = await global.getPrimaryFacilityId();
      const { data } = await global.apollo.client.query({
        query: facilityDocumentsQuery,
        variables: {
          facilityId: primaryFacilityId, // ID!
          // types: '', // [DocumentType]
          // service: '', // String
          // subService: '', // String
          // offset: '', // Int
          // limit: '', // Int
        },
      });
      expect(data.facility).toBeDefined();
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    }
  });
});

