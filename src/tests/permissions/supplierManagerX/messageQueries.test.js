// Queries
import messagesQuery from '../../../graphql/queries/messages/messagesQuery.graphql';

describe('Test "Supplier Manager" user permissions on message queries.', () => {

  global.runBeforeAll(global.supplierManagerEmail, global.plainPassword);
  global.runAfterAll();

  test('messagesQuery - Supplier Manager - should NOT return data', async () => {
    try {
      const { data } = await global.apollo.client.query({
        query: messagesQuery,
        // variables: {
        //   offset: '', // Int
        //   limit: '', // Int
        // }
      });
      expect(data.messages).toBeDefined();
      expect(data.messages).toHaveLength(0);
    } catch(error) {
      // expect(error.message).toContain('Access denied');
      expect(error).toBeUndefined();
    };
  });

});